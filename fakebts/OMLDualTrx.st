"
 Create a SM, BTS for a dual trx sceneriao

 (C) 2012 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

SiteManagerOML subclass: DualTrxSiteManager [
    <category: 'BTS-OML-DualTRX'>

    initialize [
        <category: 'creation'>
        bts := DualTrxBTSOML new
                parent: self;
                yourself.
    ]
]

BTSOML subclass: DualTrxBTSOML [
    | rc_two baseband_two |
    <category: 'BTS-OML-DualTRX'>

    availableTrx [
        <category: 'accessing'>
        ^ 2
    ]

    initialize [
        <category: 'creation'>
        super initialize.

        rc_two := RadioCarrierOML new
                    parent: self;
                    id: 2;
                    yourself.
        baseband_two := BasebandTransceiverOML new
                    parent: self;
                    id: 2;
                    yourself.
    ]

    radioCarrier: nr [
        <category: 'accessing'>
        nr = 1 ifTrue: [^radio_carrier].
        nr = 2 ifTrue: [^rc_two].
        ^ self error: 'RC(%1) not available' % {nr}.
    ]

    basebandTransceiver: nr [
        <category: 'accessing'>
        nr = 1 ifTrue: [^baseband].
        nr = 2 ifTrue: [^baseband_two].
        ^ self error: 'Baseband(%1) not available' % {nr}.
    ]

    start [
        <category: 'accessing'>
        attributes := nil.
        self basicStart.
        (self basebandTransceiver: 1) start.
        (self radioCarrier: 1) start.
        (self basebandTransceiver: 2) start.
        (self radioCarrier: 2) start.
    ]

    findObject: fomKey [
        | bb |
        <category: 'accessing'>
        self fomKey = fomKey
            ifTrue: [^self].

        fomKey second = radio_carrier class objectClass
            ifTrue: [
                | rc |
                rc := self radioCarrier: (fomKey first trx + 1).
                ^ rc findObject: fomKey].

        bb := self basebandTransceiver: (fomKey first trx + 1).
        ^ bb findObject: fomKey.
    ]
]
