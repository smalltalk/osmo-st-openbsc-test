"
 (C) 2012 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

PackageLoader fileInPackage: #FakeBTS.

FakeBTS.OMLBTSInstanceInit subclass: NACKBTSInit [
    <import: OsmoGSM>
    <comment: 'I will respond with a nack...'>

    waitForAttributes [
        | msg res nack |
        <category: 'protected'>

        msg := queue next.
        msg omDataField class = OMLSetBTSAttributes
            ifFalse: [self error: 'Failed to get SetBTSAttributes'].
        nack := msg createResponse: false.
        omlInit forwardOML: nack toMessage.
    ]
]

FakeBTS.OMLBTSInit subclass: NACKInit [
    <import: OsmoGSM>
    <comment: 'I am an INIT that will nack the BTS Attributes to check
    what OpenBSC will do'>

    initBtsInstance: aBts withQueue: aQueue [
        "Called to initialize the BTS Object"
        ^ NACKBTSInit on: aBts withInit: self withQueue: aQueue.
    ]
]

FakeBTS.BTS subclass: NACKBTS [
    | oml_gone |
    <import: OsmoGSM>
    <comment: 'I am a BTS that will NACK the OML init and this should
    cause this BTS to be dropped.'>

    NACKBTS class >> omlInitClass [
        ^ NACKInit
    ]

    connect: aHost [
        self stop.
        rsl := nil.

        oml_gone := Semaphore new.
        ^ super connect: aHost.
    ]

    omlStopped [
        oml_gone signal.
    ]

    waitForOMLGone [
        "Wait until the OML connection is gone"
        oml_gone wait.
    ]
]

Eval [
    | btsAck btsNack test lchan |

    "Connect and wait for the BTS to be ready."
    btsAck := FakeBTS.BTS new
                btsId: '1801/0/0'; connect: 'localhost';
                waitForBTSReady; yourself.

    "Now connect a NACK bts.."
    btsNack := NACKBTS new
                btsId: '1802/0/0'; connect: 'localhost';
                waitForOMLGone; yourself.

    "Verify that the first BTS is still connected"
    test := FakeBTS.OpenBSCTest initWith: btsAck.
    lchan := test requireAnyChannel.

    lchan isNil
        ifTrue: [Transcript nextPutAll: 'FAILED TO ALLOCATE A LCHAN'; nl.]
        ifFalse: [Transcript nextPutAll: 'Test passed'; nl.].
]
