PackageLoader
    fileInPackage: #OsmoNetwork;
    fileInPackage: #SUnit.

TestCase subclass: NATAuthTest [
    | socket demuxer mux queue |
    <comment: 'I test the authentication/by-passing of auth on the NAT.'>
    <import: Osmo>

    connect [
        | msg |

        socket ifNotNil: [socket close].
        socket := Sockets.Socket remote: '127.0.0.1' port: 5000.
        queue := SharedQueue new.
        demuxer := IPADemuxer initOn: socket.
        mux := IPAMuxer initOn: queue.

        "ID ACK"
        msg := demuxer next.
        self assert: msg = (Array with: 254 with: $<6> asString).

        "ID Req"
        msg := demuxer next.
        self assert: msg first = IPAConstants protocolIPA.
        self assert: msg second first asInteger = IPAConstants msgIdGet.

        "RSIP for MGCP.."
        msg := demuxer next.
        self assert: msg first = IPAConstants protocolMGCP.
    ]

    run [
        self
            testByPass;
            testShort.
    ]

    verifyNotConnected [
        [ | msg |
            msg := demuxer next.
            self assert: false.
        ] on: SystemExceptions.EndOfStream do: [^true].
    ]

    testByPass [
        | resp |
        Transcript nextPutAll: 'Testing by-pass'; nl.
        self connect.

        "Now construct a response.."
        resp := MessageBuffer new
                    putByte: IPAConstants msgIdResp;
                    putLen16: 0;
                    putByte: IPAConstants idtagUnitName;
                    yourself.
        mux nextPut: resp asByteArray with: IPAConstants protocolIPA.
        socket nextPutAllFlush: queue next.

        self verifyNotConnected.
    ]

    testShort [
        | resp |
        Transcript nextPutAll: 'Testing short'; nl.

        self connect.

        "Now construct a short message..."
        resp := MessageBuffer new
                    putByte: IPAConstants msgIdResp;
                    putLen16: 3;
                    putByte: IPAConstants idtagUnitName;
                    putByteArray: 'tes' asByteArray;
                    yourself.
        mux nextPut: resp asByteArray with: IPAConstants protocolIPA.
        socket nextPutAllFlush: queue next.

        self verifyNotConnected.
    ]
]

Eval [
    | test |
    test := NATAuthTest new
                run.
]
