# Copyright (C) 2012 Holger Hans Peter Freyther
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import dbus

class Sim(object):
    def __init__(self, bus, path):
        self.bus = bus
        self.path = path
        self.sim = dbus.Interface(bus.get_object('org.ofono', path), 'org.ofono.SimManager')

    def imsi(self):
        res = self.sim.GetProperties(['SubscriberIdentity'])
        try:
            return str(res['SubscriberIdentity'])
        except:
            return None

    def present(self):
        """The Wavecom driver is broken and 'detects' a SIM when there is None"""
        return self.imsi() != None

    def __repr__(self):
        return "<Sim(imsi=%s) of '%s'>" % (self.imsi(), self.path)

def get(bus, path):
    """Get the SIM manager"""
    return Sim(bus, path)

