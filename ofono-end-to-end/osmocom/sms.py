# Copyright (C) 2012 Holger Hans Peter Freyther
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import dbus

class Sms(object):
    def __init__(self, bus, name):
        self.bus = bus
        self.name = name
        self.sms = dbus.Interface(
                    bus.get_object('org.ofono', name),
                    'org.ofono.Message')
    def cancel(self):
        self.sms.Cancel()

    def state(self):
        return str(self.sms.GetProperties()['State'])

    def __repr__(self):
        return "<Sms('%s')>" % self.name

class SmsManager(object):
    def __init__(self, bus, name):
        self.sms = dbus.Interface(
                    bus.get_object('org.ofono', name),
                    'org.ofono.MessageManager')
        self.bus = bus
        self.name = name

    def send_message(self, number, text, delivery_report):
        self.sms.SetProperty('UseDeliveryReports', dbus.Boolean(int(delivery_report)))
        return self.sms.SendMessage(number, text)

    def all_message_names(self):
        messages = self.sms.GetMessages()
        return map(lambda x: str(x[0]), messages)

    def get_message(self, path):
        return Sms(self.bus, path)

    def registerOnMsgAdded(self, cb):
        self.sms.connect_to_signal('MessageAdded', cb)

    def registerOnMsgRemoved(self, cb):
        self.sms.connect_to_signal('MessageRemoved', cb)

    def __repr__(self):
        return "<SmsManager for Modem('%s')>" % self.name

def get(bus, name):
    return SmsManager(bus, name)
