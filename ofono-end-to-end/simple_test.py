#!/usr/bin/env python
# Copyright (C) 2012 Holger Hans Peter Freyther
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import dbus
import dbus.mainloop.glib
import gobject
import time

from osmocom import modem

messages = []

dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
bus = dbus.SystemBus()
mod = modem.detect_modems(bus, sleep=False, poweroff=False)[0]

print mod.name

sim = mod.sim()
print sim.present()
print sim.imsi()

sms = mod.sms()
sms.send_message('2233', 'BLA', False)
print sms.all_message_names()
for name in sms.all_message_names():
    msg = sms.get_message(name)
    print msg.state()
    print msg
    msg.cancel()

#watcher = sms.SmsWatcher(sm)
#for i in range(1, 2000):
#    messages.append(sms.sendessage(sm, '39323', 'TEST %d' % i, False))
    #time.sleep(2)
    #sms.wait_for_sent(messages)


mainloop = gobject.MainLoop()
print dir(mainloop)
mainloop.run()
