#!/usr/bin/env python
# Copyright (C) 2012 Holger Hans Peter Freyther
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Hi. I am going to send SMS between all available modules and then wait
# for it to complete and verify that all messages arrived and find out
# which one is missing.

from osmocom.nitb_test import NitbTest

class SmsMassTest(NitbTest):
    def __init__(self, host, sms_to_send=1000):
        NitbTest.__init__(self, host)
        self.sms_to_send = sms_to_send

    def run_test(self):
        """Run the test"""

        self.clear_all_sms()

        extensions = self.ext2mod.keys()
        for nr in xrange(1, self.sms_to_send):
            for modem in self.avail_modems:
                self.send_sms(nr, modem, extensions)

        # Now wait.... TODO. We could connect to the MessageAdded/Removed
        # signal and enter the event loop here...
        if len(self.avail_modems) > 0:
            print("Queued a lot of SMS... now waiting.")
        else:
            print("No SMS queued due lack of modems.")

    def clear_all_sms(self):
        # Clear all SMS so we can easily verify we get SMS..
        for modem in self.avail_modems:
            sms = modem.sms()
            for msg in sms.all_message_names():
                try:
                    sms.get_message(msg).cancel()
                except:
                    print("Deleting SMS('%s') failed. Continuing." % msg)

    def send_sms(self, nr, modem, extension_lists):
        # Send a SMS to all extensions
        sms = modem.sms()
        for extension in extension_lists:
            # Do not send a SMS to myself (unless this wants to be tested)
            if self.ext2mod[extension] == modem:
                continue

            text = "This is SMS %d from %s to %s" % (nr, self.mod2ext[modem], extension)
            sms.send_message(extension, text, False)

if __name__ == "__main__":
    SmsMassTest('localhost').run()
