#!/bin/bash

set -ex

cd openbsc
#./contrib/jenkins.sh
export LD_LIBRARY_PATH=$PWD/deps/install/lib/

cd ../systemtest
rm -f hlr.sqlite3
./systemtest.py
